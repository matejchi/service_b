# frozen_string_literal: true

require 'test_helper'

class TwitterMailerTest < ActionMailer::TestCase
  setup do
    @user = users(:bruce)
    @result_record = results(:result_record)
    @data = "<p><b>Tweet URL: </b><a href=''></a></p><p><b>INFO:</b> RT @GerberKawasaki: First thoughts driving my new Tesla model Y."\
      "Review and discussion of what I learned at Tesla today and outlook for the…</p><p><b>DATE:</b> Thu Jun 25 22:41:52 +0000 2020"\
      "</p><p><b>Urls:</b></p>----------------------------------------------------------------------------------<p>Best regards</p>"
  end

  context '#send_email' do
    should 'send_email when invoked with correct params' do
      TwitterMailer.send_email(@result_record, @data).deliver_now
      assert_not ActionMailer::Base.deliveries.empty?
    end

    should 'sent email should use settings configured in the email template' do
      template = EmailTemplate.create(template_name: 'test',
                                      sender_name: 'matej',
                                      subject: 'testing',
                                      user_id: @user.id,
                                      body: 'Hi {{user_name}} Here are your tweeter URLs {{tweets}}')

      @result_record.update!(email_template_id: template.id)

      email = TwitterMailer.send_email(@result_record, @data).deliver_now

      assert_equal template.sender_name, email.from.first
      assert_equal @result_record.input['receiver_email'], email.to.first
      assert_equal template.subject, email.subject
    end
  end
end
