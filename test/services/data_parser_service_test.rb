# frozen_string_literal: true

require 'test_helper'

class DataParserServiceTest < ActiveSupport::TestCase
  context '#twitter' do
    should 'return parsed tweets data' do
      file = File.read('test/fixtures/files/tweets.json')
      json_data = JSON.parse(file)

      parsed_data = DataParserService.twitter(json_data)

      assert_match(/Tweet URL:/, parsed_data)
      assert_match(/INFO:/, parsed_data)
      assert_match(/DATE:/, parsed_data)
      assert_match(/Urls:/, parsed_data)
      assert_match(/Best regards/, parsed_data)
      assert_match(/#{json_data[0].dig('entities', 'urls', 0, 'url')}/, parsed_data)
      assert_match(/#{json_data[0].dig('text')}/, parsed_data)
    end

    should 'return empty template if json data is empty' do
      parsed_data = DataParserService.twitter([])

      assert_match(/No data to show for selected period/, parsed_data)
    end
  end
end
