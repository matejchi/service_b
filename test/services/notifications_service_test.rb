# frozen_string_literal: true

require 'test_helper'

class NotificationsServiceTest < ActiveSupport::TestCase
  context '#call' do
    setup do
      @result_record = results(:result_2)
    end

    should 'invoke mailer' do
      service = NotificationsService.new(result_id: @result_record.id)
      service.expects(:send_email).once
      service.call
    end
  end
end
