# frozen_string_literal: true

require 'test_helper'

class NotificationsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @headers = { 'X-SERVICE-A-IDENTIFIER' => ENV['SERVICE_A_HEADER_SECRET'] }
  end

  context 'INCOMING' do
    should 'be successful' do
      post notifications_url, params: { result_id: 1 }, headers: @headers
      assert_response :success
    end

    should 'raise error if params are wrong' do
      assert_raises ActiveRecord::RecordNotFound do
        post notifications_url, params: { result_id: 22 }, headers: @headers
      end
    end

    should 'return error message and HTTP status code 400, if Service A Header Identifier is missing or wrong' do
      post notifications_url, params: { result_id: 1 }, headers: {}

      assert_response :bad_request
      assert_equal 'Missing or wrong Service A identifier', response.parsed_body['error']

      post notifications_url, params: { result_id: 1 }, headers: @headers.merge('X-SERVICE-A-IDENTIFIER' => 'wrong_secret_key')

      assert_response :bad_request
      assert_equal 'Missing or wrong Service A identifier', response.parsed_body['error']
    end

    should 'invoke sending of email' do
      assert_difference 'ActionMailer::Base.deliveries.size', +1 do
        post notifications_url, params: { result_id: 1 }, headers: @headers
      end
    end
  end
end
