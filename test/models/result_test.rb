# frozen_string_literal: true

require 'test_helper'

class ResultTest < ActiveSupport::TestCase
  context 'associations' do
    should belong_to(:user).class_name('User')
    should belong_to(:email_template).class_name('EmailTemplate')
  end

  context 'validations' do
    should validate_presence_of(:user)
    should validate_presence_of(:email_template)
  end
end
