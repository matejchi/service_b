# frozen_string_literal: true

require 'test_helper'

class EmailTemplateTest < ActiveSupport::TestCase
  context 'associations' do
    should belong_to(:user).class_name('User')
    should have_many(:results).class_name('Result')
  end

  context 'validations' do
    should validate_presence_of(:template_name)
    should validate_uniqueness_of(:template_name)
    should validate_presence_of(:user)
    should validate_inclusion_of(:email_type).in_array(EmailTemplate::EMAIL_TYPES).with_message('Email type not supported')
  end
end
