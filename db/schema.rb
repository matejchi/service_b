# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_06_27_113104) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "email_templates", force: :cascade do |t|
    t.string "template_name", null: false
    t.string "email_type", default: "twitter"
    t.string "sender_name"
    t.string "subject"
    t.text "body"
    t.string "description"
    t.bigint "user_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["user_id"], name: "index_email_templates_on_user_id"
  end

  create_table "results", force: :cascade do |t|
    t.jsonb "downloaded_data", default: {}
    t.jsonb "input", default: {}
    t.bigint "email_template_id"
    t.bigint "user_id"
    t.integer "status", default: 0
    t.datetime "sent_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["email_template_id"], name: "index_results_on_email_template_id"
    t.index ["user_id"], name: "index_results_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", null: false
    t.string "name", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["email"], name: "index_users_on_email", unique: true
  end

  add_foreign_key "email_templates", "users"
  add_foreign_key "results", "email_templates"
  add_foreign_key "results", "users"
end
