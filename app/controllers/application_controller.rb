# frozen_string_literal: true

class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  rescue_from ActionController::BadRequest, with: :bad_request

  private

  def bad_request
    render json: { error: 'Missing or wrong Service A identifier' }, status: :bad_request
  end
end
