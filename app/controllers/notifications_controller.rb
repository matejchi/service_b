# frozen_string_literal: true

class NotificationsController < ApplicationController
  skip_before_action :verify_authenticity_token

  SERVICE_A_HEADER_SECRET = ENV.fetch('SERVICE_A_HEADER_SECRET', '')

  def incoming
    check_header
    NotificationsService.new(params).call
    head :ok
  end

  private

  def check_header
    raise ActionController::BadRequest unless request.headers['X-SERVICE-A-IDENTIFIER'] == SERVICE_A_HEADER_SECRET
  end
end
