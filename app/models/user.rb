# frozen_string_literal: true

class User < ApplicationRecord
  EMAIL_REGEX = /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i

  has_many :email_templates, dependent: :delete_all # do not validate anything, just delete
  has_many :results, dependent: :delete_all

  validates :name, presence: true
  validates :email, presence: true, uniqueness: true
  validates :email, format: { with: EMAIL_REGEX }
end
