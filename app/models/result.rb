# frozen_string_literal: true

class Result < ApplicationRecord
  enum status: [:created, :downloaded, :sent, :failed]

  belongs_to :user
  belongs_to :email_template

  validates :user, :email_template, presence: true
end
