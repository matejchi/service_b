# frozen_string_literal: true

class NotificationsService
  def initialize(params)
    @result_record = Result.includes(:email_template).find(params[:result_id])
    @email_type = @result_record.email_template.email_type
  end

  def call
    process_results
    send_email
  rescue StandardError => e
    Rails.logger.error("ERROR while processing notification: #{e.inspect}")
  end

  private

  def process_results
    @parsed_tweets_data = DataParserService.public_send(@email_type, @result_record.downloaded_data)
  end

  def send_email
    mailer = "#{@email_type}Mailer".camelize.constantize
    mailer.send_email(@result_record, @parsed_tweets_data).deliver_now
  end
end
