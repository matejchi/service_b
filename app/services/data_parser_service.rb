# frozen_string_literal: true

module DataParserService
  class << self
    def twitter(data)
      return 'No data to show for selected period' unless data.present?

      parsed_data = []

      data.each do |tweet|
        tweet_url = tweet.dig('entities', 'urls', 0, 'url')
        text = tweet.dig('text')
        date = tweet.dig('created_at')
        urls = text.scan(/\S+\.\S+/)

        parsed_data << "<p><b>Tweet URL: </b><a href='#{tweet_url}'>#{tweet_url}</a></p>"
        parsed_data << "<p><b>INFO:</b> #{text}</p>"
        parsed_data << "<p><b>DATE:</b> #{date}</p>"
        parsed_data << "<p><b>Urls:</b></p>"

        urls.each do |url|
          parsed_data << "<p>#{url}</p>"
        end

        parsed_data << "----------------------------------------------------------------------------------"
      end

      parsed_data << "<p>Best regards</p>"
      parsed_data.join('')
    end

    def linkedin(data)
      raise 'not implemented'
    end
  end
end
