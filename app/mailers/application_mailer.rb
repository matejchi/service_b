class ApplicationMailer < ActionMailer::Base
  default from: 'no-reply@service-b.com'
  layout 'mailer'
end
