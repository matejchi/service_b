# frozen_string_literal: true

class TwitterMailer < ApplicationMailer
  def send_email(result_record, data)
    user = result_record.user.name
    tweets = data
    email_template = result_record.email_template
    body = email_template.body.gsub("{{user_name}}", user).gsub("{{tweets}}", tweets)

    mail(to: result_record.input['receiver_email'], subject: email_template.subject, from: email_template.sender_name) do |format|
      format.html { render html: body.html_safe }
    end
  end
end
