**SERVICE B**

* **Service B**
  - It will 'send'/open emails in web browser, by using 'letter_opener' gem. No real sending of emails will be done.

* **Dependencies:**
  - Ruby v 2.7.1
  - Rails 6.0.3
  - PostgreSQL > 9.3
  - .env file is included in commit. It's an exception of the rule, since this is an exercise.


* **To setup Service B run:**
  - bundle install
  - bundle exec rails db:prepare RAILS_ENV=test
  - bundle exec rails s -p 3005

* **For tests run:**
  - bundle exec rails test


