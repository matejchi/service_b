# frozen_string_literal: true

Rails.application.routes.draw do
  post 'notifications', to: 'notifications#incoming'
end
